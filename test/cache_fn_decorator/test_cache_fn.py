import pytest
from simple_caching.storage import DictMemory
from simple_caching import cache_fn

counter = 0

@pytest.fixture(autouse=True)
def run_around_tests():
    TestCacheFnAPI.counter = 0
    yield

@cache_fn(DictMemory)
def set_fn(x):
    TestCacheFnAPI.counter += 1
    return x**2

@cache_fn(DictMemory, key_encode_fn=lambda x: x+1)
def set_fn_2(x):
    TestCacheFnAPI.counter += 1
    return x**2

@cache_fn(DictMemory, key_encode_fn=lambda x: 1)
def set_fn_bad_cache(x):
    TestCacheFnAPI.counter += 1
    return x**2

class TestCacheFnAPI:
    counter = 0

    def test_set_1(self):
        assert set_fn(2) == 4
        assert TestCacheFnAPI.counter == 1

    def test_set_2(self):
        assert set_fn_2(2) == 4
        assert set_fn_2(2) == 4
        assert TestCacheFnAPI.counter == 1

    def test_set_3(self):
        assert set_fn_bad_cache(2) == 4
        assert set_fn_bad_cache(100) == 4
        assert TestCacheFnAPI.counter == 1
