import pytest
from simple_caching.storage import DictMemory
from simple_caching.cache_fn_decorator.cache_function_call import CacheFunctionCall

counter = 0

@pytest.fixture(autouse=True)
def run_around_tests():
    TestCacheFnAPI.counter = 0
    yield

def set_fn(x):
    TestCacheFnAPI.counter += 1
    return x**2

class TestCacheFnAPI:
    counter = 0

    def test_ctor_1(self):
        cache = CacheFunctionCall(DictMemory("test"), set_fn)
        assert not cache is None

    def test_set_1(self):
        cache = CacheFunctionCall(DictMemory("test"), set_fn)
        cache.set_value(2)
        assert TestCacheFnAPI.counter == 1

    def test_set_2(self):
        """Cache Fn set is lazy always"""
        cache = CacheFunctionCall(DictMemory("test"), set_fn)
        cache.set_value(2)
        cache.set_value(2)
        assert TestCacheFnAPI.counter == 1

    def test_get_vs_getitem_1(self):
        """get(encoded_key) vs __getitem__(key) difference. get assumes key is encoded."""
        cache = CacheFunctionCall(DictMemory("test", lambda x: x+1), set_fn)
        cache.set_value(2)
        assert cache.base_cache.get_key(3) == 4
        assert TestCacheFnAPI.counter == 1

    def test_get_vs_getitem_2(self):
        """get(encoded_key) vs __getitem__(key) difference. __getitem__ encodes the key beforehand."""
        cache = CacheFunctionCall(DictMemory("test", lambda x: x+1), set_fn)
        cache.set_value(2)
        assert cache[2] == 4
        assert TestCacheFnAPI.counter == 1

    def test_check_vs_contains_1(self):
        """check(encoded_key) vs __contains__(key) difference. check assumes key is encoded."""
        cache = CacheFunctionCall(DictMemory("test", lambda x: x+1), set_fn)
        cache.set_value(2)
        assert cache.base_cache.check_key(3) == True
        assert cache.base_cache.check_key(2) == False

    def test_check_vs_contains_2(self):
        """check(encoded_key) vs __contains__(key) difference. ___contains__ encodes the key beforehand."""
        cache = CacheFunctionCall(DictMemory("test", lambda x: x+1), set_fn)
        cache.set_value(2)
        assert 3 not in cache
        assert 2 in cache
