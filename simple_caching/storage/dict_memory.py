"""Dict memory module. Caches data in the memory in a dictionary."""
from typing import Optional
from overrides import overrides

from ..cache import Cache, EncodedKeyType, ValueType
from ..logger import logger

class DictMemory(Cache):
    """DictMemory implementation"""
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.cache = {}

    @overrides
    def set_value(self, encoded_key: EncodedKeyType, value: ValueType):
        self.cache[encoded_key] = value

    @overrides
    def get_key(self, encoded_key: EncodedKeyType) -> Optional[ValueType]:
        return self.cache[encoded_key]

    @overrides
    def check_key(self, encoded_key: EncodedKeyType) -> bool:
        return encoded_key in self.cache

    @overrides
    def clear(self):
        logger.debug(f"Clearing cache {len(self.cache.keys())} keys")
        self.cache = {}

    def __str__(self) -> str:
        f_str = "[DictMemory]"
        f_str += f" - Num cached keys: {len(self.cache.keys())}"
        return f_str

    def __repr__(self) -> str:
        return self.__str__()
